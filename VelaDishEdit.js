"use strict";

function newDishButtonClicked (clickedButton) {
	var upperLevelHasBeenSaved = false;
	if (clickedButton.parentNode.className == "sectionHeaderDiv") {
		upperLevelHasBeenSaved = saveSectionButtonClicked(clickedButton.parentNode);
		removeNewSubsectionButton(clickedButton);
	} else {
		upperLevelHasBeenSaved = saveSubsectionButtonClicked(clickedButton.parentNode);
	}
	if (upperLevelHasBeenSaved) {
		if (hasSubsection(clickedButton)) {
			alert("You already have a dish under the current section. Dish and Subsection cannot be in the same level :(");
		} else {
			var sectionDiv = clickedButton.parentNode.parentNode.parentNode;

			var dishDiv = document.createElement('div');
			dishDiv.className = "dishDiv";

			var dishHeaderDiv = createDishHeaderDiv();

			var dishImageAndDetailsDiv = createDishImageAndDetailsDiv();

			dishDiv.appendChild(dishHeaderDiv);
			dishDiv.appendChild(dishImageAndDetailsDiv);
			sectionDiv.appendChild(dishDiv);
		}
	}
}


function removeNewSubsectionButton(clickedButton) {
	var newSubsectionButton = clickedButton.previousSibling;
	clickedButton.parentNode.removeChild(newSubsectionButton);
}

function hasSubsection(clickedButton) {
	var sectionInfoDiv = clickedButton.parentNode.parentNode;
	var currentDiv = sectionInfoDiv.nextSibling;
	while (currentDiv != null) {
		if (currentDiv.className == "subsectionDiv") {
			return true;
		}
		currentDiv = currentDiv.nextSibling;
	}
	return false;
}

function createDishHeaderDiv () {
	var div = document.createElement('div');
	div.className = "DishHeaderDiv";

	var nameInput = document.createElement('input');
	nameInput.className = "DishNameInput";
	nameInput.placeholder = "Dish Name";
	nameInput.addEventListener("blur", function( event ) {
		saveDishButtonClicked(div);
	}, true);

	var priceInput = document.createElement('input');
	priceInput.className = "DishPriceInput";
	priceInput.placeholder = "Dish Price";

	var saveButton = document.createElement('button');
	saveButton.onclick = function() {saveDishButtonClicked(div);};
	saveButton.innerHTML = "Save Dish";


	var deleteButton = document.createElement('button');
	deleteButton.onclick = function() {deleteDishButtonClicked(div)};
	deleteButton.innerHTML = "Delete Dish";

	div.appendChild(nameInput);
	div.appendChild(priceInput);
	div.appendChild(saveButton);
	div.appendChild(deleteButton);

	return div;
}


function createDishImageAndDetailsDiv() {
	var imageAndDetailsDiv = document.createElement('div');

	var canvas = document.createElement('canvas');
	var img = new Image();
	img.onload = function () {
		canvas.width = 100;
		canvas.height = 100;
		var ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);
	}
	img.src = "Vela.jpeg";

	var imageUploadButton = document.createElement('input');
	imageUploadButton.type = "file";
	imageUploadButton.onchange = function() {previewImage(imageUploadButton);};

	var details = document.createElement('textarea');

	details.placeholder = "Dish Details";

	imageAndDetailsDiv.appendChild(canvas);
	imageAndDetailsDiv.appendChild(imageUploadButton);
	imageAndDetailsDiv.appendChild(details);

	return imageAndDetailsDiv;
}

function saveDishButtonClicked(dishHeaderDiv) {
	if (DishNameIsEmpty(dishHeaderDiv)) {
		alert("Please enter a name for this dish before moving on :)");
	} else if (theNameHasBeenTaken(dishHeaderDiv.firstChild.value)) {
		alert("This name has been taken already. Please choose another name");
	} else {
		displaySavingDivWhileSaving();
		markDeleteDishButtonForSaving(dishHeaderDiv);
		if (dishHeaderDiv.parentNode.id == "") {
			saveNewDish(dishHeaderDiv);
		} else {
			saveUpdatedDish(dishHeaderDiv);
		}
	}
}

function markDeleteDishButtonForSaving(dishHeaderDiv) {
	var deleteButton = dishHeaderDiv.lastChild;
	deleteButton.className = "saving";
}

function unmarkDeleteDishButtonForSaving(dishHeaderDiv) {
	var deleteButton = dishHeaderDiv.lastChild;
	deleteButton.className = "";
}

function DishNameIsEmpty(dishHeaderDiv)  {
	return dishHeaderDiv.firstChild.value == "";
}

function saveUpdatedDish(dishHeaderDiv) {
	var Dish = Parse.Object.extend("Dish");
	var query = new Parse.Query(Dish);
	query.get(dishHeaderDiv.parentNode.id, {
	  success: function(dish) {
	    saveDish(dish, dishHeaderDiv);
	  },
	  error: function(object, error) {
	  	hideSavingDiv();
	  	unmarkDeleteDishButtonForSaving(dishHeaderDiv);
	  	alert('Failed to update this dish, with error code: ' + error.message);
	    // The object was not retrieved successfully.
	    // error is a Parse.Error with an error code and message.
	  }
	});
}

function saveNewDish(dishHeaderDiv) {
	var Dish = Parse.Object.extend("Dish");	
	var dish = new Dish();
	saveDish(dish, dishHeaderDiv);
}

function saveDish(dish, dishHeaderDiv) {
	var name = dishHeaderDiv.firstChild.value;
	dish.set("name", name);

	var price = dishHeaderDiv.childNodes[1].value;
	dish.set("price", price);

	var sectionName = dishHeaderDiv.parentNode.parentNode.firstChild.firstChild.firstChild.value;
	dish.set("section", sectionName);

	var details = dishHeaderDiv.nextSibling.childNodes[2].value;
	dish.set("details", details);

	var restaurantName = document.getElementsByClassName("restaurantDiv")[0].id;
	dish.set("restaurant", restaurantName);

	var username = "v";
	dish.set("username", username);

	if (dishHeaderDiv.nextSibling.childNodes[1].files[0] != null) {		
		var file = 	dishHeaderDiv.nextSibling.childNodes[1].files[0];
		var canvas = dishHeaderDiv.nextSibling.firstChild;

		// Create thumbnail for this image
		canvas.width = 100;
        canvas.height = 100;

		var img = new Image();
		img.src = canvas.value;
        
        var ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);
        
        var smallImage = new Parse.File("smallImage_" + file.name, {base64: canvas.toDataURL()});
        dish.set("smallImage", smallImage); 

		var largeImage = dishHeaderDiv.nextSibling.childNodes[1].files[0];
 		dish.set("largeImage", new Parse.File("largeImage_" + file.name, largeImage));
	}

	dish.save(null, {
		success: function(dish) {
			displaySavingDivAfterSaved();
			setTimeout(hideSavingDiv, 3000);
		    dishHeaderDiv.parentNode.id = dish.id;
		  	unmarkDeleteDishButtonForSaving(dishHeaderDiv);
		  },
		  error: function(dish, error) {
		  	hideSavingDiv();
	  		unmarkDeleteDishButtonForSaving(dishHeaderDiv);
		    alert('Failed to save the dish, with error code: ' + error.message);
		  }
		});
}

function deleteDishButtonClicked(dishHeaderDiv) {
	if (confirm('Are you sure you want to delete this Dish ?')) {
		if (dishHeaderDiv.lastChild.className == "") {
			if (dishHeaderDiv.parentNode.id == "") {
				displayDeletingDivWhileDeleting();
				var sectionDiv = dishHeaderDiv.parentNode.parentNode; 
				sectionDiv.removeChild(dishHeaderDiv.parentNode);
				displayDeletingDivAfterDeleted();
				setTimeout(hideDeletingDiv, 3000);

			} else {
				displayDeletingDivWhileDeleting();
				var Dish = Parse.Object.extend("Dish");
				var query = new Parse.Query(Dish);
				query.get(dishHeaderDiv.parentNode.id, {
				  success: function(dish) {
				    dish.destroy({
					  success: function(dish) {
						var sectionDiv = dishHeaderDiv.parentNode.parentNode; 
						sectionDiv.removeChild(dishHeaderDiv.parentNode);
						displayDeletingDivAfterDeleted();
						setTimeout(hideDeletingDiv, 3000);
					    // alert("Section has been deleted from remote database => need deleting and deleted div like saving");
					  },
					  error: function(dish, error) {
					    alert("Fail to delete the dish with error = " + error);
					    hideDeletingDiv();
					  }
					});
				  },
				  error: function(dish, error) {
				    // The object was not retrieved successfully.
				    // error is a Parse.Error with an error code and message.
				    alert("Fail to delete the dish with error = " + error);
				    hideDeletingDiv();
				  }
				});
			}
		} else {
			alert("Cannot delete this section while saving is still processing");
		}
	}
}
