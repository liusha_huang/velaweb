"use strict";

function loadRestaurantsForCurrentUser() {
	var Restaurant = Parse.Object.extend("Restaurant");
	var query = new Parse.Query(Restaurant);
	query.equalTo("username", "v");
	query.find({
		success: function(restaurants) {
		    if (restaurants.length != 0) {
		    	initRestaurantTabs(restaurants);
		    	loadRestaurants(restaurants);
		    }
		},
		error: function(object, error) {
			alert("Fail to fetch restaurants :(");
		    // The object was not retrieved successfully.
		    // error is a Parse.Error with an error code and message.
		}
	});
}

function loadRestaurants(restaurants) {
	for (var i = 0; i < restaurants.length; i++) {
		loadRestaurantDataWithName(restaurants[i].get("name"));
	}
}

function loadRestaurantDataWithName(restaurantName) {
	var Section = Parse.Object.extend("Section");
	var query = new Parse.Query(Section);
	query.equalTo("restaurant", "Grill 'Em");
	query.find({
		success: function(sections) {
		    if (sections.length != 0) {
		    	loadSectionData(sections)
		    }
		},
		error: function(object, error) {
			alert("Fail to fetch sections for restaurant " + restaurantName);
		    // The object was not retrieved successfully.
		    // error is a Parse.Error with an error code and message.
		}
	});
}

function loadSectionData(sections) {
	var sectionGroup = buildSubsectionStructure(sections);
	var groupNameList = getSortedGroupNameList(sectionGroup);
	for (var i = 0; i < groupNameList.length; i++){
		if (sectionGroup[groupNameList[i]].length == 1) {
			// This group has no subsection, only has dish as sub-level
			addSectionDivWithDishesForSection(sectionGroup[groupNameList[i]][0]);
		} else {
			// This group has subsection, only has subsection as sub-level
			addSectionDivWithSubsectionsForSection(groupNameList[i], sectionGroup[groupNameList[i]]);
		}
	}
}

function getSortedGroupNameList(sectionGroup) {
	var groupNameList = Object.keys(sectionGroup);
	var sortedGroupNameList = new Array(groupNameList.length);
	for (var i = 0; i < groupNameList.length; i++) {
		var groupName = groupNameList[i];
		var section = sectionGroup[groupName][0];
		if (!contains(sortedGroupNameList, groupName)) {
			sortedGroupNameList[section.get("groupIndex")] = groupName;
		}
	}
	return sortedGroupNameList;
}

function buildSubsectionStructure(sections) {
	var sectionGroup = {};
	for (var i = 0; i < sections.length; i++){
		if (contains(Object.keys(sectionGroup), sections[i].get("groupName"))) {
			sectionGroup[sections[i].get("groupName")].push(sections[i]);
		} else {
			var newSectionList = [];
			newSectionList.push(sections[i]);
			sectionGroup[sections[i].get("groupName")] = newSectionList;
		}
	}
	return sectionGroup;
}

function addSectionDivWithSubsectionsForSection(groupName, subsections) {
	var restaurantDiv = document.getElementsByClassName("restaurantDiv")[0];

	var sectionDiv = createSectionDivWithGroupName(groupName);

	for (var i = 0; i < subsections.length; i++) {
		addSubsectionDivForSubsection(subsections[i], sectionDiv);
	}

	restaurantDiv.appendChild(sectionDiv);
}

function createSectionDivWithGroupName(groupName) {
	var sectionDiv = document.createElement('div');
	sectionDiv.className = "sectionDiv";
	
	var sectionInfoDiv = createNewSectionInfoDivWithGroupName(groupName);

	sectionDiv.appendChild(sectionInfoDiv);

	return sectionDiv;
}

function createNewSectionInfoDivWithGroupName (groupName) {
	var infoDiv = document.createElement('div');
	infoDiv.className = "sectionInfoDiv";

	var sectionHeaderDiv = createSectionHeaderDivWithGroupName(groupName);

	infoDiv.appendChild(sectionHeaderDiv);
	return infoDiv;
}

function createSectionHeaderDivWithGroupName (groupName) {
	var div = document.createElement('div');
	div.className = "sectionHeaderDiv";
	div.id = groupName; // save old groupName in id when the value is changed in input text

	var nameInput = document.createElement('input');
	nameInput.className = "sectionNameInput";
	nameInput.value = groupName;
	nameInput.addEventListener("blur", function( event ) {
		saveSectionButtonClicked(div);
	}, true);

	var newSubsectionButton = document.createElement('button');
	newSubsectionButton.onclick = function() {newSubsectionButtonClicked(newSubsectionButton);};
	newSubsectionButton.innerHTML = "Add a new Sub-section";

	var saveButton = document.createElement('button');
	saveButton.onclick = function() {saveSectionButtonClicked(div);};
	saveButton.innerHTML = "Save Section";

	var deleteButton = document.createElement('button');
	deleteButton.onclick = function() {deleteSectionButtonClicked(div)};
	deleteButton.innerHTML = "Delete Section";

	div.appendChild(nameInput);
	div.appendChild(newSubsectionButton);
	div.appendChild(saveButton);
	div.appendChild(deleteButton);

	return div;
}


function addSubsectionDivForSubsection(subsection, sectionDiv) {
	var subsectionDiv = document.createElement('div');
	subsectionDiv.className = "subsectionDiv";
	subsectionDiv.id = subsection.id;

	var subsectionInfoDiv = addSubsectionInfoDivForSubsection(subsection);
	subsectionDiv.appendChild(subsectionInfoDiv);

	sectionDiv.appendChild(subsectionDiv);

	var Dish = Parse.Object.extend("Dish");
	var query = new Parse.Query(Dish);
	query.equalTo("restaurant", "Grill 'Em");
	query.equalTo("section", subsection.get("name"));
	query.find({
		success: function(dishes) {
		    if (dishes.length != 0) {
		    	loadDishData(dishes, subsectionDiv);
		    }
		},
		error: function(object, error) {
			alert("Fail to fetch dish for section " + subsection.get("name"));
		    // The object was not retrieved successfully.
		    // error is a Parse.Error with an error code and message.
		}
	});
}

function addSubsectionInfoDivForSubsection(subsection) {
	var infoDiv = document.createElement('div');
	infoDiv.className = "subsectionInfoDiv"

	var subsectionHeaderDiv = addSubsectionHeaderDivForSubsection(subsection);

	var subsectionImageAndDetailsDiv = addSubsectionImageAndDetailsDivForSubsection(subsection);

	infoDiv.appendChild(subsectionHeaderDiv);
	infoDiv.appendChild(subsectionImageAndDetailsDiv);

	return infoDiv;
}

function addSubsectionHeaderDivForSubsection(subsection) {
	var div = document.createElement('div');
	div.className = "subsectionHeaderDiv";

	var nameInput = document.createElement('input');
	nameInput.className = "subsectionNameInput";
	nameInput.value = subsection.get("name");
	nameInput.addEventListener("blur", function( event ) {
		saveSubsectionButtonClicked(div);
	}, true);

	var newSubsectionButton = document.createElement('button');
	newSubsectionButton.onclick = function() {newSubsectionButtonClicked(newDishButton);};
	newSubsectionButton.innerHTML = "Add a new Subsection";

	var newDishButton = document.createElement('button');
	newDishButton.onclick = function() {newDishButtonClicked(newDishButton);};
	newDishButton.innerHTML = "Add a new Dish";

	var saveButton = document.createElement('button');
	saveButton.onclick = function() {saveSubsectionButtonClicked(div);};
	saveButton.innerHTML = "Save Subection";

	var deleteButton = document.createElement('button');
	deleteButton.onclick = function() {deleteSubsectionButtonClicked(div)};
	deleteButton.innerHTML = "Delete Subsection";

	div.appendChild(nameInput);
	div.appendChild(newSubsectionButton);
	div.appendChild(newDishButton);
	div.appendChild(saveButton);
	div.appendChild(deleteButton);

	return div;
}

function addSubsectionImageAndDetailsDivForSubsection(subsection) {
	var imageAndDetailsDiv = document.createElement('div');

	var canvas = document.createElement('canvas');
	var img = new Image();
		img.onload = function () {
			canvas.width = 100;
			canvas.height = 100;
			var ctx = canvas.getContext('2d');
			ctx.drawImage(img, 0, 0);
		}
	if (subsection.get("smallImage") != null) {
		img.src = subsection.get("smallImage").url();
	} else {
		img.src = "Vela.jpeg";
	}


	var imageUploadButton = document.createElement('input');
	imageUploadButton.type = "file";
	imageUploadButton.onchange = function() {previewImage(imageUploadButton);};

	var details = document.createElement('textarea');

	details.value = subsection.get("details");

	imageAndDetailsDiv.appendChild(canvas);
	imageAndDetailsDiv.appendChild(imageUploadButton);
	imageAndDetailsDiv.appendChild(details);

	return imageAndDetailsDiv;
}


function addSectionDivWithDishesForSection(section) {
	var restaurantDiv = document.getElementsByClassName("restaurantDiv")[0];

	var sectionDiv = document.createElement('div');
	sectionDiv.className = "sectionDiv";
	sectionDiv.id = section.id;

	var sectionInfoDiv = addSectionInfoDivForSection(section);
	sectionDiv.appendChild(sectionInfoDiv);

	restaurantDiv.appendChild(sectionDiv);

	var Dish = Parse.Object.extend("Dish");
	var query = new Parse.Query(Dish);
	query.equalTo("restaurant", "Grill 'Em");
	query.equalTo("section", section.get("name"));
	query.find({
		success: function(dishes) {
		    if (dishes.length != 0) {
		    	loadDishData(dishes, sectionDiv);
		    	removeNewSubsectionButtonFromSectionDiv(sectionDiv);
		    }
		},
		error: function(object, error) {
			alert("Fail to fetch dish for section " + section.get("name"));
		    // The object was not retrieved successfully.
		    // error is a Parse.Error with an error code and message.
		}
	});
}

function removeNewSubsectionButtonFromSectionDiv(sectionDiv) {
	var sectionHeaderDiv = sectionDiv.firstChild.firstChild;
	var newSubsectionButton = sectionHeaderDiv.childNodes[1];
	sectionHeaderDiv.removeChild(newSubsectionButton);
}

function addSectionInfoDivForSection(section) {
	var infoDiv = document.createElement('div');
	infoDiv.className = "sectionInfoDiv";

	var sectionHeaderDiv = addSectionHeaderDivForSection(section);

	var sectionImageAndDetailsDiv = addSectionImageAndDetailsDivForSection(section);

	infoDiv.appendChild(sectionHeaderDiv);
	infoDiv.appendChild(sectionImageAndDetailsDiv);
	return infoDiv;
}

function addSectionHeaderDivForSection(section) {
	var div = document.createElement('div');
	div.className = "sectionHeaderDiv";

	var nameInput = document.createElement('input');
	nameInput.className = "sectionNameInput";
	nameInput.value = section.get("name");
	nameInput.addEventListener("blur", function( event ) {
		saveSectionButtonClicked(div);
	}, true);

	var newSubsectionButton = document.createElement('button');
	newSubsectionButton.onclick = function() {newSubsectionButtonClicked(newSubsectionButton);};
	newSubsectionButton.innerHTML = "Add a new Sub-section";

	var newDishButton = document.createElement('button');
	newDishButton.onclick = function() {newDishButtonClicked(newDishButton);};
	newDishButton.innerHTML = "Add a new Dish";

	var saveButton = document.createElement('button');
	saveButton.onclick = function() {saveSectionButtonClicked(sectionHeaderDiv);};
	saveButton.innerHTML = "Save Section";

	var deleteButton = document.createElement('button');
	deleteButton.onclick = function() {deleteSectionButtonClicked(sectionHeaderDiv)};
	deleteButton.innerHTML = "Delete Section";


	div.appendChild(nameInput);
	div.appendChild(newSubsectionButton);
	div.appendChild(newDishButton);
	div.appendChild(saveButton);
	div.appendChild(deleteButton);

	return div;
}

function addSectionImageAndDetailsDivForSection(section) {
	var imageAndDetailsDiv = document.createElement('div');

	var canvas = document.createElement('canvas');
	var img = new Image();
		img.onload = function () {
			canvas.width = 100;
			canvas.height = 100;
			var ctx = canvas.getContext('2d');
			ctx.drawImage(img, 0, 0);
		}
	if (section.get("smallImage") != null) {
		img.src = section.get("smallImage").url();
	} else {
		img.src = "Vela.jpeg";
	}

	var imageUploadButton = document.createElement('input');
	imageUploadButton.type = "file";
	imageUploadButton.onchange = function() {previewImage(imageUploadButton);};

	var details = document.createElement('textarea');
	details.value = section.get("details");

	imageAndDetailsDiv.appendChild(canvas);
	imageAndDetailsDiv.appendChild(imageUploadButton);
	imageAndDetailsDiv.appendChild(details);

	return imageAndDetailsDiv;
}

function loadDishData(dishes, sectionDiv) {
	for (var i = 0; i < dishes.length; i++) {
		addDishDivForDish(dishes[i], sectionDiv);
	}
}

function addDishDivForDish(dish, sectionDiv) {
	var dishDiv = document.createElement('div');
	dishDiv.className = "dishDiv";
	dishDiv.id = dish.id;

	var dishHeaderDiv = addDishHeaderDivForDish(dish);

	var dishImageAndDetailsDiv = addDishImageAndDetailsDivForDish(dish);

	dishDiv.appendChild(dishHeaderDiv);
	dishDiv.appendChild(dishImageAndDetailsDiv);
	sectionDiv.appendChild(dishDiv);
}


function addDishHeaderDivForDish (dish) {
	var div = document.createElement('div');
	div.className = "DishHeaderDiv";

	var nameInput = document.createElement('input');
	nameInput.className = "DishNameInput";
	nameInput.value = dish.get("name");	
	nameInput.addEventListener("blur", function( event ) {
		saveDishButtonClicked(div);
	}, true);

	var priceInput = document.createElement('input');
	priceInput.className = "DishPriceInput";
	priceInput.value = dish.get("price");

	var saveButton = document.createElement('button');
	saveButton.onclick = function() {saveDishButtonClicked(div);};
	saveButton.innerHTML = "Save Dish";

	var deleteButton = document.createElement('button');
	deleteButton.onclick = function() {deleteDishButtonClicked(div)};
	deleteButton.innerHTML = "Delete Dish";

	div.appendChild(nameInput);
	div.appendChild(priceInput);
	div.appendChild(saveButton);
	div.appendChild(deleteButton);

	return div;
}

function addDishImageAndDetailsDivForDish(dish) {
	var imageAndDetailsDiv = document.createElement('div');

	var canvas = document.createElement('canvas');
	var img = new Image();
		img.onload = function () {
			canvas.width = 100;
			canvas.height = 100;
			var ctx = canvas.getContext('2d');
			ctx.drawImage(img, 0, 0);
		}
	if (dish.get("smallImage") != null) {
		img.src = dish.get("smallImage").url();
	} else {
		img.src = "Vela.jpeg";
	}
	
	var imageUploadButton = document.createElement('input');
	imageUploadButton.type = "file";
	imageUploadButton.onchange = function() {previewImage(imageUploadButton);};


	var details = document.createElement('textarea');
	details.value = dish.get("details");

	imageAndDetailsDiv.appendChild(canvas);
	imageAndDetailsDiv.appendChild(imageUploadButton);
	imageAndDetailsDiv.appendChild(details);

	return imageAndDetailsDiv;
}

function contains(array, element) {
	if (array == null) {
		return false;
	}
	for (var i = 0; i < array.length; i++) {
		if (array[i] == element) {
			return true;
		}
	}
	return false;
}







































