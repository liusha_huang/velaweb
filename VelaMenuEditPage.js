"use strict";

function previewImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var canvas = getNewCanvasWithInputTag(input);
        reader.onload = function (e) {
        	var img = new Image();
        	img.src = e.target.result;

			var ctx = canvas.getContext('2d');
			canvas.width = 100;
			canvas.height = 100;
			ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height)
			canvas.value = canvas.toDataURL();
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function getNewCanvasWithInputTag(input) {
	var parent = input.parentNode;
    parent.removeChild(parent.firstChild);
    var canvas = document.createElement('canvas');
    parent.insertBefore(canvas, input);
    return canvas;
}


function theNameHasBeenTaken(name) {
	var sectionNames = getSectionNames();

	var subsectionNames = getSubsectionNames();

	var dishNames = getDishNames();

	return (containsMoreThanOne(sectionNames, name) || containsMoreThanOne(subsectionNames, name) || containsMoreThanOne(dishNames, name));
}

function getSectionNames() {
	var sections = document.getElementsByClassName("sectionNameInput");
	var sectionNames = [];
	for (var i = 0; i < sections.length; i++) {
		sectionNames[i] = sections[i].value;
	}
	return sectionNames;
}

function getSubsectionNames() {
	var subsections = document.getElementsByClassName("subsectionNameInput");
	var subsectionNames = [];
	for (var i = 0; i < subsections.length; i++) {
		subsectionNames[i] = subsections[i].value;
	}
	return subsectionNames;
}

function getDishNames() {
	var dishes = document.getElementsByClassName("DishNameInput");
	var dishNames = [];
	for (var i = 0; i < dishes.length; i++) {
		dishNames[i] = dishes[i].value;
	}
	return dishNames;
}

function containsMoreThanOne(array, name) {
	var count = 0;
	for (var i = 0; i < array.length; i++) {
		if (array[i].toLowerCase() == name.toLowerCase()) {
			count ++;
		}
	}
	return count >= 2;
}

function displaySavingDivWhileSaving() {
	var savingDiv = document.getElementById('savingDiv');
	savingDiv.innerHTML = "Saving ...";
	savingDiv.style.visibility = "visible";
}

function displaySavingDivAfterSaved() {
	var savingDiv = document.getElementById('savingDiv');
	savingDiv.innerHTML = "All changes have been saved :)";
	savingDiv.style.visibility = "visible";
}

function hideSavingDiv() {
	var savingDiv = document.getElementById('savingDiv');
	savingDiv.style.visibility = "hidden";
}

function displayDeletingDivWhileDeleting() {
	var savingDiv = document.getElementById('deletingDiv');
	savingDiv.innerHTML = "Deleting...";
	savingDiv.style.visibility = "visible";
}

function displayDeletingDivAfterDeleted() {
	var savingDiv = document.getElementById('deletingDiv');
	savingDiv.innerHTML = "This item has been deleted";
	savingDiv.style.visibility = "visible";
}

function hideDeletingDiv() {
	var savingDiv = document.getElementById('deletingDiv');
	savingDiv.style.visibility = "hidden";
}

function pageDidLoad() {
	Parse.initialize("lUCTXtUYPdE622BrlNFKJ2uOaCVp0vLdb0vwrsV4", "Y8W5u5oqJjmhfdd8h7FS1aLYoCMbVbcXgbwNmX9s");
	loadRestaurantsForCurrentUser();
}

window.onload = pageDidLoad;