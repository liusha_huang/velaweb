"use strict";

var tabLinks = new Array();
var contentDivs = new Array();

function initRestaurantTabs(restaurants) {
	buildTabs(restaurants);

	buildRestaurantDivs(restaurants);

	enableTabFunctionalities();
}

function enableTabFunctionalities() {

	buildTabRelatedDataStructure();

  	assignOnClickEvents();
	
	hideContentDivs();
}

function buildTabs(restaurants) {
	var ul = document.createElement("ul");
	ul.id = "tabs"
	for (var i = 0; i < restaurants.length; i++) {
		ul.appendChild(buildTabForRestaurant(restaurants[i].get("name")));
	}
	document.body.appendChild(ul);
}

function buildTabForRestaurant(restaurantName) {
	var li = document.createElement("li");

	var a = document.createElement("a");
	a.setAttribute("href", "#" + restaurantName);
	a.innerHTML = restaurantName;

	li.appendChild(a);

	return li;
}

function buildRestaurantDivs(restaurants) {
	for (var i = 0; i < restaurants.length; i++) {
		var restaurantDiv = buildRestaurantDivForRestaurant(restaurants[i].get("name"));
		document.body.appendChild(restaurantDiv);
	}
}

function buildRestaurantDivForRestaurant(restaurantName) {
	var restaurantDiv = document.createElement("div");
	restaurantDiv.className = "restaurantDiv";
	restaurantDiv.id = restaurantName;

	var restaurantInfoDiv = document.createElement("div");
	restaurantInfoDiv.className = "restaurantInfoDiv";

	var header = document.createElement("h1");
	header.innerHTML = restaurantName;

	var button = document.createElement("button");
	button.className = "newSection";
	button.onclick = function () {newSectionButtonClicked(button)};
	button.innerHTML = "Add a new section";

	restaurantInfoDiv.appendChild(header);
	restaurantInfoDiv.appendChild(button);

	restaurantDiv.appendChild(restaurantInfoDiv);

	return restaurantDiv;
}


function addNewRestaurantButtonClicked(clickedButton) {
	var ul = document.getElementById("tabs"); 

	var restaurantName = "restaurant" + ul.childNodes.length;

	ul.appendChild(buildTabForRestaurant(restaurantName));

	var restaurantDiv = buildRestaurantDivForRestaurant(restaurantName);
	document.body.appendChild(restaurantDiv);

	buildTabRelatedDataStructure();

  	assignOnClickEventsForRestaurant(restaurantName);

  	hideRestaurantDivForRestaurant(restaurantName);

}

function buildTabRelatedDataStructure() {
	tabLinks = new Array();
	contentDivs = new Array();
	var tabListItems = document.getElementById('tabs').childNodes;
	for ( var i = 0; i < tabListItems.length; i++ ) {
		if ( tabListItems[i].nodeName == "LI" ) {
		  var tabLink = getFirstChildWithTagName( tabListItems[i], 'A' );
		  var id = getHash( tabLink.getAttribute('href') );
		  tabLinks[id] = tabLink;
		  contentDivs[id] = document.getElementById( id );
		}
	}
}

function assignOnClickEvents() {
	var i = 0;
	for (var id in tabLinks ) {
		tabLinks[id].onclick = showTab;
		tabLinks[id].onfocus = function() { this.blur() };
		if ( i == 0 ) {
			tabLinks[id].className = 'selected'; // High light the selected from css
		}
		i++;
	}
}

function assignOnClickEventsForRestaurant(restaurantName) {
	tabLinks[restaurantName].onclick = showTab;
	tabLinks[restaurantName].onfocus = function() { this.blur() };
}

function hideRestaurantDivForRestaurant(restaurantName) {
	contentDivs[restaurantName].className = 'restaurantDiv hide';
}

function hideContentDivs() {
	var i = 0;
	for ( var id in contentDivs ) {
		if ( i != 0 ) {
			contentDivs[id].className = 'restaurantDiv hide';
		}
	i++;
	}
}


function getFirstChildWithTagName( element, tagName ) {
  for ( var i = 0; i < element.childNodes.length; i++ ) {
    if ( element.childNodes[i].nodeName == tagName ) {
    	return element.childNodes[i];
    }
  }
}

function showTab() {
  var selectedId = getHash( this.getAttribute('href') );

  // Highlight the selected tab, and dim all others.
  // Also show the selected content div, and hide all others.
  for ( var id in contentDivs ) {
    if ( id == selectedId ) {
      tabLinks[id].className = 'selected';
      contentDivs[id].className = 'restaurantDiv';
    } else {
      tabLinks[id].className = '';
      contentDivs[id].className = 'restaurantDiv hide';
    }
  }

  // Stop the browser following the link
  return false;
}

function getHash( url ) {
  var hashPos = url.lastIndexOf ( '#' );
  return url.substring( hashPos + 1 );
}