"use strict";

function newSectionButtonClicked (clickedButton) {
	var restaurantDiv = clickedButton.parentNode.parentNode;

	var sectionDiv = document.createElement('div');
	sectionDiv.className = "sectionDiv";

	var sectionInfoDiv = createNewSectionInfoDiv();

	sectionDiv.appendChild(sectionInfoDiv);

	restaurantDiv.appendChild(sectionDiv);
}

function createNewSectionInfoDiv () {
	var infoDiv = document.createElement('div');
	infoDiv.className = "sectionInfoDiv";

	var sectionHeaderDiv = createSectionHeaderDiv();

	var sectionImageAndDetailsDiv = createSectionImageAndDetailsDiv();

	infoDiv.appendChild(sectionHeaderDiv);
	infoDiv.appendChild(sectionImageAndDetailsDiv);
	return infoDiv;
}

function createSectionHeaderDiv () {
	var div = document.createElement('div');
	div.className = "sectionHeaderDiv";

	var nameInput = document.createElement('input');
	nameInput.className = "sectionNameInput";
	nameInput.placeholder = "Section Name";
	nameInput.addEventListener("blur", function( event ) {
		if (!hasSubsections(div)) {
			// Do not save section when there is subsections
			saveSectionButtonClicked(div);
		}
	}, true);


	var newSubsectionButton = document.createElement('button');
	newSubsectionButton.onclick = function() {newSubsectionButtonClicked(newSubsectionButton);};
	newSubsectionButton.innerHTML = "Add a new Sub-section";

	var newDishButton = document.createElement('button');
	newDishButton.onclick = function() {newDishButtonClicked(newDishButton);};
	newDishButton.innerHTML = "Add a new Dish";

	var saveButton = document.createElement('button');
	saveButton.onclick = function() {saveSectionButtonClicked(div);};
	saveButton.innerHTML = "Save Section";

	var deleteButton = document.createElement('button');
	deleteButton.onclick = function() {deleteSectionButtonClicked(div)};
	deleteButton.innerHTML = "Delete Section";


	div.appendChild(nameInput);
	div.appendChild(newSubsectionButton);
	div.appendChild(newDishButton);
	div.appendChild(saveButton);
	div.appendChild(deleteButton);

	return div;
}

function createSectionImageAndDetailsDiv() {
	var imageAndDetailsDiv = document.createElement('div');

	var canvas = document.createElement('canvas');
	var img = new Image();
	img.onload = function () {
		canvas.width = 100;
		canvas.height = 100;
		var ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);
	}
	img.src = "Vela.jpeg";


	var imageUploadButton = document.createElement('input');
	imageUploadButton.type = "file";
	imageUploadButton.onchange = function() {previewImage(imageUploadButton);};

	var details = document.createElement('textarea');

	details.placeholder = "Section Details";

	imageAndDetailsDiv.appendChild(canvas);
	imageAndDetailsDiv.appendChild(imageUploadButton);
	imageAndDetailsDiv.appendChild(details);

	return imageAndDetailsDiv;
}

function saveSectionButtonClicked(sectionHeaderDiv) {
	if (sectionNameIsEmpty(sectionHeaderDiv)) {
		alert("Please enter a name for this section before moving on :)");
		return false;
	} else if (theNameHasBeenTaken(sectionHeaderDiv.firstChild.value)) {
		alert("This name has been taken already. Please choose another name");
		return false;
	} else if (hasSubsections(sectionHeaderDiv)) {
		updateSubsectionGroupName(sectionHeaderDiv);
		return true;	
	} else {
		displaySavingDivWhileSaving();
		markDeleteSectionButtonForSaving(sectionHeaderDiv)
		if (sectionHeaderDiv.parentNode.parentNode.id == "") {
			saveNewSection(sectionHeaderDiv);
		} else {
			saveUpdatedSection(sectionHeaderDiv);	
		}
		return true;
	}	
} 

function markDeleteSectionButtonForSaving(sectionHeaderDiv) {
	var deleteButton = sectionHeaderDiv.lastChild;
	deleteButton.className = "saving";
}

function unmarkDeleteSectionButtonForSaving(sectionHeaderDiv) {
	var deleteButton = sectionHeaderDiv.lastChild;
	deleteButton.className = "";
}

function sectionNameIsEmpty(sectionHeaderDiv) {
	return sectionHeaderDiv.firstChild.value == "";
}

function saveNewSection(sectionHeaderDiv) {
	var Section = Parse.Object.extend("Section");	
	var section = new Section();
	saveSection(section, sectionHeaderDiv);
}

function saveUpdatedSection(sectionHeaderDiv) {
	var Section = Parse.Object.extend("Section");
	var query = new Parse.Query(Section);
	query.get(sectionHeaderDiv.parentNode.parentNode.id, {
		success: function(section) {
			var newSectionName = sectionHeaderDiv.firstChild.value;
			if (newSectionName != section.get("name") && hasSubitems(sectionHeaderDiv)) {
				// The section name is changed so all dishes or subsection under this section 
				// need to update the section name for this change
				// Do not update the section until all dishes have been updated
				updateDishSectionName(section);
			} else {
		    	saveSection(section, sectionHeaderDiv);
			}
		},
		error: function(object, error) {
			hideSavingDiv();
			unmarkDeleteSectionButtonForSaving(sectionHeaderDiv);
	   		alert('Failed to update this section, with error code: ' + error.message);
		    // The object was not retrieved successfully.
		    // error is a Parse.Error with an error code and message.
		}
	});
}

function hasSubitems(sectionHeaderDiv) {
	return (sectionHeaderDiv.parentNode.parentNode.childNodes.length > 1);
}

function hasDish(sectionHeaderDiv) {
	return (sectionHeaderDiv.parentNode.parentNode.childNodes.length > 1 &&
		    sectionHeaderDiv.parentNode.parentNode.childNodes[1].className == "dishDiv");
}

function saveSection(section, sectionHeaderDiv) {
	var sectionDiv = sectionHeaderDiv.parentNode.parentNode;

	var name = sectionHeaderDiv.firstChild.value;
	section.set("name", name);

	var groupName = name;
	section.set("groupName", name);

	var groupIndex = getGroupIndexForSection(groupName, sectionDiv);
	section.set("groupIndex", groupIndex);

	section.set("sectionIndex", groupIndex);

	var details = sectionHeaderDiv.nextSibling.childNodes[2].value;
	section.set("details", details);

	var restaurantName = document.getElementsByClassName("restaurantDiv")[0].id;
	section.set("restaurant", restaurantName);

	var username = "v";
	section.set("username", username);

	if (sectionHeaderDiv.nextSibling.childNodes[1].files[0] != null) {		
		var file = 	sectionHeaderDiv.nextSibling.childNodes[1].files[0];
		var canvas = sectionHeaderDiv.nextSibling.firstChild;

		// Create thumbnail for this image
		canvas.width = 100;
        canvas.height = 100;

		var img = new Image();
		img.src = canvas.value;
        
        var ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);
        
        var smallImage = new Parse.File("smallImage_" + file.name, {base64: canvas.toDataURL()});
        section.set("smallImage", smallImage); 

		var largeImage = sectionHeaderDiv.nextSibling.childNodes[1].files[0];
 		section.set("largeImage", new Parse.File("largeImage_" + file.name, largeImage));
	} 
	section.save(null, {
		success: function(section) {
			displaySavingDivAfterSaved();
			setTimeout(hideSavingDiv, 3000);
			sectionHeaderDiv.parentNode.parentNode.id = section.id;
			unmarkDeleteSectionButtonForSaving(sectionHeaderDiv);
	  	},
	  	error: function(section, error) {
			hideSavingDiv();
	   		alert('Failed to create new object, with error code: ' + error.message);
			unmarkDeleteSectionButtonForSaving(sectionHeaderDiv);
  		}
	});
}

function getGroupIndexForSection(groupName, sectionDiv) {
	var restaurantDiv = sectionDiv.parentNode;
	for (var i = 1; i < restaurantDiv.childNodes.length; i++){
		var currentSectionDiv = restaurantDiv.childNodes[i];
		var sectionName = currentSectionDiv.firstChild.firstChild.firstChild.value;
		if (sectionName == groupName) {
			return i - 1;
		}
	}
}

function updateSubsectionGroupName(sectionHeaderDiv) {
	var Section = Parse.Object.extend("Section");
	var query = new Parse.Query(Section);
	query.equalTo("restaurant", "Grill 'Em");
	query.equalTo("groupName", sectionHeaderDiv.id);
	query.find({
		success: function(subsections) {
		    if (subsections.length != 0) {
		    	for (var i = 0; i < subsections.length; i++) {
		    		var id = subsections[i].id;
		    		var subsectionDiv = document.getElementById(subsections[i].id);
		    		saveSubsection(subsections[i], subsectionDiv.firstChild.firstChild);
				}	
		    }
		},
		error: function(object, error) {
		    // The object was not retrieved successfully.
		    // error is a Parse.Error with an error code and message.
		}
	});	
}

function updateDishSectionName(section) {
	// start from here to get all dishes info and update the related section name
	// to the new one 
	// only update the section when all dishes are updated !!!
	var Dish = Parse.Object.extend("Dish");
	var query = new Parse.Query(Dish);
	query.equalTo("restaurant", "Grill 'Em");
	query.equalTo("section", section.get("name"));
	query.find({
		success: function(dishes) {
		    if (dishes.length != 0) {
		    	for (var i = 0; i < dishes.length; i++) {
		    		var dishDiv = document.getElementById(dishes[i].id);
		    		saveDish(dishes[i], dishDiv.firstChild);
				}	
		    }
		    // This is not safe!! 
		    // In order to make sure everything updated correctly, I need some server side
		    // code support to update all dishes before I updated the section
		    // The good thing about server side code is that I do not need to worry about
		    // network issue, even parse SDK should handle that pretty well.
		    saveSection(section, document.getElementById(section.id).firstChild.firstChild);
		},
		error: function(object, error) {
		    // The object was not retrieved successfully.
		    // error is a Parse.Error with an error code and message.
		}
	});
}

function hasSubsections(sectionHeaderDiv) {
	return sectionHeaderDiv.parentNode.childNodes.length == 1;
}

function deleteSectionButtonClicked(sectionHeaderDiv) {
	if (confirm('Are you sure you want to delete this section including the data under this section?')) {
	    if (sectionHeaderDiv.lastChild.className == "") {
			if (sectionHeaderDiv.parentNode.parentNode.id == "") {
				displayDeletingDivWhileDeleting();
				var restaurantDiv = document.getElementsByClassName("restaurantDiv"); 
				restaurantDiv[0].removeChild(sectionHeaderDiv.parentNode.parentNode);
				displayDeletingDivAfterDeleted();
				setTimeout(hideDeletingDiv, 3000);
			} else {
				displayDeletingDivWhileDeleting();
				var Section = Parse.Object.extend("Section");
				var query = new Parse.Query(Section);
				query.get(sectionHeaderDiv.parentNode.parentNode.id, {
				  success: function(section) {
				    section.destroy({
					  success: function(section) {
					  	var restaurantDiv = document.getElementsByClassName("restaurantDiv"); 
						restaurantDiv[0].removeChild(sectionHeaderDiv.parentNode.parentNode);
						displayDeletingDivAfterDeleted();
						setTimeout(hideDeletingDiv, 3000);
					    // alert("Section has been deleted from remote database => need deleting and deleted div like saving");
					  },
					  error: function(section, error) {
					    alert("Fail to delete the section with error = " + error);
					    hideDeletingDiv();
					  }
					});
				  },
				  error: function(section, error) {
				    // The object was not retrieved successfully.
				    // error is a Parse.Error with an error code and message.
				    alert("Fail to get section with error = " + error);
				    hideDeletingDiv();
				  }
				});
			}
		} else {
			alert("Cannot delete this section while saving is still processing");
		}
	}
}