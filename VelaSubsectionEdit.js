"use strict";

function newSubsectionButtonClicked (clickedButton) {
	if (hasDish(clickedButton)) {
		alert("You already have a dish under the current section. Dish and Subsection cannot be in the same level :(");
	} else {	
		var sectionDiv = clickedButton.parentNode.parentNode.parentNode;
		var subsectionInfoDiv = createNewSubsectionInfoDiv();

		var subsectionDiv = document.createElement('div');
		subsectionDiv.className = "subsectionDiv";

		subsectionDiv.appendChild(subsectionInfoDiv);

		sectionDiv.appendChild(subsectionDiv);

		removeNewDishButton(clickedButton);

		// Do not need real section info for group name
		removeSectionImageAndDetailsDiv(clickedButton.parentNode.parentNode);
	}
}

function removeSectionImageAndDetailsDiv(sectionDiv) {
	sectionDiv.firstChild.id = sectionDiv.firstChild.firstChild.value;
	sectionDiv.removeChild(sectionDiv.childNodes[1]);
}

function removeNewDishButton(clickedButton) {
	var newDishButton = clickedButton.nextSibling;
	clickedButton.parentNode.removeChild(newDishButton);
}

function hasDish(clickedButton) {
	var sectionInfoDiv = clickedButton.parentNode.parentNode;
	var currentDiv = sectionInfoDiv.nextSibling;
	while (currentDiv != null) {
		if (currentDiv.className == "dishDiv") {
			return true;
		}
		currentDiv = currentDiv.nextSibling
	}
	return false;
}

function createNewSubsectionInfoDiv () {
	var infoDiv = document.createElement('div');
	infoDiv.className = "subsectionInfoDiv"

	var subsectionHeaderDiv = createSubsectionHeaderDiv();

	var subsectionImageAndDetailsDiv = createSubsectionImageAndDetailsDiv();

	infoDiv.appendChild(subsectionHeaderDiv);
	infoDiv.appendChild(subsectionImageAndDetailsDiv);

	return infoDiv;
}

function createSubsectionHeaderDiv () {
	var div = document.createElement('div');
	div.className = "subsectionHeaderDiv";

	var nameInput = document.createElement('input');
	nameInput.className = "subsectionNameInput";
	nameInput.placeholder = "Subsection Name";
	nameInput.addEventListener("blur", function( event ) {
		saveSubsectionButtonClicked(div);
	}, true);

	var newDishButton = document.createElement('button');
	newDishButton.onclick = function() {newDishButtonClicked(newDishButton);};
	newDishButton.innerHTML = "Add a new Dish";

	var saveButton = document.createElement('button');
	saveButton.onclick = function() {saveSubsectionButtonClicked(div);};
	saveButton.innerHTML = "Save Subection";

	var deleteButton = document.createElement('button');
	deleteButton.onclick = function() {deleteSubsectionButtonClicked(div)};
	deleteButton.innerHTML = "Delete Subsection";

	div.appendChild(nameInput);
	div.appendChild(newDishButton);
	div.appendChild(saveButton);
	div.appendChild(deleteButton);

	return div;
}

function createSubsectionImageAndDetailsDiv() {
	var imageAndDetailsDiv = document.createElement('div');

	var canvas = document.createElement('canvas');
	var img = new Image();
	img.onload = function () {
		canvas.width = 100;
		canvas.height = 100;
		var ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);
	}
	img.src = "Vela.jpeg";

	var imageUploadButton = document.createElement('input');
	imageUploadButton.type = "file";
	imageUploadButton.onchange = function() {previewImage(imageUploadButton);};

	var details = document.createElement('textarea');

	details.placeholder = "Subsection Details";

	imageAndDetailsDiv.appendChild(canvas);
	imageAndDetailsDiv.appendChild(imageUploadButton);
	imageAndDetailsDiv.appendChild(details);

	return imageAndDetailsDiv;
}

function saveSubsectionButtonClicked(subsectionHeaderDiv)  {
	if (subsectionNameIsEmpty(subsectionHeaderDiv)) {
		alert("Please enter a name for this subsection before moving on :)");
		return false;
	} else if (theNameHasBeenTaken(subsectionHeaderDiv.firstChild.value)) {
		alert("This name has been taken already. Please choose another name");
		return false;
	} else {
		displaySavingDivWhileSaving();
		markDeleteSubsectionButtonForSaving(subsectionHeaderDiv);
		if (subsectionHeaderDiv.parentNode.parentNode.id == "") {
			saveNewSubsection(subsectionHeaderDiv);
		} else {
			saveUpdatedSubsection(subsectionHeaderDiv);
		}
		return true;
	}
}


function markDeleteSubsectionButtonForSaving(subsectionHeaderDiv) {
	var deleteButton = subsectionHeaderDiv.lastChild;
	deleteButton.className = "saving";
}

function unmarkDeleteSubsectionButtonForSaving(subsectionHeaderDiv) {
	var deleteButton = subsectionHeaderDiv.lastChild;
	deleteButton.className = "";
}

function subsectionNameIsEmpty(subsectionHeaderDiv)  {
	return subsectionHeaderDiv.firstChild.value == "";
}

function saveNewSubsection(subsectionHeaderDiv) {
	var Section = Parse.Object.extend("Section");	
	var section = new Section();
	saveSubsection(section, subsectionHeaderDiv);
}

function saveUpdatedSubsection(subsectionHeaderDiv) {
	var Section = Parse.Object.extend("Section");
	var query = new Parse.Query(Section);
	query.get(subsectionHeaderDiv.parentNode.parentNode.id, {
		success: function(section) {
		    var newSectionName = subsectionHeaderDiv.firstChild.value;
			if (newSectionName != section.get("name") && hasDishes(subsectionHeaderDiv)) {
				updateDishSubsectionName(section);
			} else {
		    	saveSection(section, subsectionHeaderDiv);
			}

		},
		error: function(object, error) {
			hideSavingDiv();
			unmarkDeleteSubsectionButtonForSaving(subsectionHeaderDiv);
			alert('Failed to update the subsection, with error code: ' + error.message);
		  
		    // The object was not retrieved successfully.
		    // error is a Parse.Error with an error code and message.
		}
	});
}

function hasDishes(subsectionHeaderDiv) {
	return (subsectionHeaderDiv.parentNode.parentNode.childNodes.length > 1);
}

function saveSubsection(section, subsectionHeaderDiv) {
	var name = subsectionHeaderDiv.firstChild.value;
	section.set("name", name);

	var groupName = subsectionHeaderDiv.parentNode.parentNode.parentNode.firstChild.firstChild.firstChild.value;
	section.set("groupName", groupName);

	var groupIndex = getGroupIndexForSection(groupName, subsectionHeaderDiv.parentNode.parentNode.parentNode);
	section.set("groupIndex", groupIndex);

	var sectionIndex = getSubsectionIndexForSubsection(name, subsectionHeaderDiv.parentNode.parentNode);
	section.set("sectionIndex", sectionIndex);

	var details = subsectionHeaderDiv.nextSibling.childNodes[2].value;
	section.set("details", details);

	var restaurantName = document.getElementsByClassName("restaurantDiv")[0].id;
	section.set("restaurant", restaurantName);

	var username = "v";
	section.set("username", username);

	// Get image from canvas
	if (subsectionHeaderDiv.nextSibling.childNodes[1].files[0] != null) {	
		var file = 	subsectionHeaderDiv.nextSibling.childNodes[1].files[0];
		var canvas = subsectionHeaderDiv.nextSibling.firstChild;

		// Create thumbnail for this image
		canvas.width = 100;
        canvas.height = 100;

		var img = new Image();
		img.src = canvas.value;
		
        var ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);
        
        var smallImage = new Parse.File("smallImage_" + file.name, {base64: canvas.toDataURL()});
        section.set("smallImage", smallImage); 

		var largeImage = new Parse.File("largeImage_" + file.name, file);
 		section.set("largeImage", largeImage);
	} 

	section.save(null, {
		success: function(section) {
			displaySavingDivAfterSaved();
			setTimeout(hideSavingDiv, 3000);
			subsectionHeaderDiv.parentNode.parentNode.id = section.id;
			unmarkDeleteSubsectionButtonForSaving(subsectionHeaderDiv);

		  },
		  error: function(section, error) {
		  	hideSavingDiv();
			unmarkDeleteSubsectionButtonForSaving(subsectionHeaderDiv);
		    alert('Failed to create new object, with error code: ' + error.message);
		  }
	});
}

function getSubsectionIndexForSubsection(subsectionName, subsectionDiv) {
	var mainSectionDiv = subsectionDiv.parentNode;
	for (var i = 1; i < mainSectionDiv.childNodes.length; i++){
		var currentSubsectionDiv = mainSectionDiv.childNodes[i];
		var currentSubsectionName = currentSubsectionDiv.firstChild.firstChild.firstChild.value;
		if (currentSubsectionName == subsectionName) {
			return i - 1;
		}
	}
}

function updateDishSubsectionName(section) {
	// start from here to get all dishes info and update the related section name
	// to the new one 
	// only update the section when all dishes are updated !!!
	var Dish = Parse.Object.extend("Dish");
	var query = new Parse.Query(Dish);
	query.equalTo("restaurant", "Grill 'Em");
	query.equalTo("section", section.get("name"));
	query.find({
		success: function(dishes) {
		    if (dishes.length != 0) {
		    	for (var i = 0; i < dishes.length; i++) {
		    		var dishDiv = document.getElementById(dishes[i].id);
		    		saveDish(dishes[i], dishDiv.firstChild);
				}	
		    }
		    // This is not safe!! 
		    // In order to make sure everything updated correctly, I need some server side
		    // code support to update all dishes before I updated the section
		    // The good thing about server side code is that I do not need to worry about
		    // network issue, even parse SDK should handle that pretty well.
		    saveSubsection(section, document.getElementById(section.id).firstChild.firstChild);
		},
		error: function(object, error) {
			hideSavingDiv();
		    alert('Failed to update the subsection, with error code: ' + error.message);
		  
		    // The object was not retrieved successfully.
		    // error is a Parse.Error with an error code and message.
		}
	});
}



function deleteSubsectionButtonClicked(subsectionHeaderDiv) {
	if (confirm('Are you sure you want to delete this section including the data under this section?')) {
		if (subsectionHeaderDiv.lastChild.className == "") {
			if (subsectionHeaderDiv.parentNode.parentNode.id == "") {
				displayDeletingDivWhileDeleting();
				var sectionDiv = subsectionHeaderDiv.parentNode.parentNode.parentNode; 
				sectionDiv.removeChild(subsectionHeaderDiv.parentNode.parentNode);
				displayDeletingDivAfterDeleted();
				setTimeout(hideDeletingDiv, 3000);
			} else {
				displayDeletingDivWhileDeleting();
				var Section = Parse.Object.extend("Section");
				var query = new Parse.Query(Section);
				query.get(subsectionHeaderDiv.parentNode.parentNode.id, {
				  success: function(section) {
				    section.destroy({
					  success: function(section) {
						var sectionDiv = subsectionHeaderDiv.parentNode.parentNode.parentNode; 
						sectionDiv.removeChild(subsectionHeaderDiv.parentNode.parentNode);
						displayDeletingDivAfterDeleted();
						setTimeout(hideDeletingDiv, 3000);
					    // alert("Section has been deleted from remote database => need deleting and deleted div like saving");
					  },
					  error: function(section, error) {
					  	hideDeletingDiv();
					    alert("Fail to delete the subsection with error = " + error);
					    setTimeout(hideDeletingDiv, 3000);
					  }
					});
				  },
				  error: function(section, error) {
				  	hideDeletingDiv();
				    // The object was not retrieved successfully.
				    // error is a Parse.Error with an error code and message.
				    alert("Fail todelete subsection with error = " + error);
				    setTimeout(hideDeletingDiv, 3000);
				  }
				});
				
			}
		} else {
			alert("Cannot delete this subsection while saving is still processing");
		}	
	}
}